import cv2
import os

DATA_DIR = 'Data'

def get_dir(dir_name = None):
    "Get directory file names"
    if dir_name is None:
        folders = [x for x in os.listdir('Data') if os.path.isdir('/'.join((DATA_DIR, x)))]
    else:
        folders = [x for x in os.listdir('Data') if os.path.isdir('/'.join((DATA_DIR, x))) and x == dir_name]
    
    frames = {}
                   
    for folder in folders:
        frames[folder] = os.listdir('/'.join((DATA_DIR, folder)))
        frames[folder].sort()

    return frames

def read_dir(frames, prompt = False):
    "Read the specified images. Frames must be a output of get_dir()"
    images = {}
    for folder in frames:
        if prompt:
            print(folder+'---')
        imlist = []
        for frame in frames[folder]:
            if prompt:
                print(folder+'/'+frame+'...')
            imlist.append([frame, cv2.imread('/'.join((DATA_DIR, folder, frame)))])
            
        if prompt:
            print('---')
        images[folder] = imlist
    return images
                                    

def get_class(dir_name):
    "Returns class of a filename. True for forward, False for backward"
    if dir_name.startswith('F'):
        return True
    else:
        return False

def rev_name(dir_name):
    "Interchange F_* and N_*"
    assert dir_name.startswith('F_') or dir_name.startswith('B_')
    prefix = dir_name[:2]
    suffix = dir_name[2:]
    prefix = 'B_' if prefix == 'F_' else 'F_'
    return prefix + suffix

def rev_dir(frames):
    "Produce reverse sample by reversing frame order"
    r_dict = {}
    for folder in frames:
        r_folder = rev_name(folder)
        assert r_folder not in frames
        r_dict[r_folder] = frames[folder][::-1]

    frames.update(r_dict)
