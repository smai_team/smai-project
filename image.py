import numpy as np
import cv2 as cv

def get_cc_im(inp_im, disk_sz = [5, 6, 7, 8, 9]):
    "Returns the number of connected components of a difference image"
    if len(inp_im.shape) == 3:
        # convert to grayscale
        inp_im = cv.cvtColor(inp_im, cv.COLOR_BGR2GRAY)
    
    # ensure single channel is recieved
    assert len(inp_im.shape) == 2
    ret, thresh = cv.threshold(inp_im, 0, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)

    toret = list()
    for sz in disk_sz:
        struct_elem = cv.getStructuringElement(cv.MORPH_ELLIPSE, (sz, sz))
        opened_im = cv.morphologyEx(thresh, cv.MORPH_OPEN, struct_elem)
        num_comp, _ = cv.connectedComponents(opened_im, connectivity = 8)
        toret.append(num_comp)
    return toret

def image_warp(im2, im1, num_iter = 1000):
    "Image registration"

    im1_gray = cv.cvtColor(im1,cv.COLOR_BGR2GRAY)
    im2_gray = cv.cvtColor(im2,cv.COLOR_BGR2GRAY)
    sz = im1.shape

    warp_matrix = np.eye(2, 3, dtype=np.float32)
    termination_eps = 1e-5;
    criteria = (cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, num_iter,  termination_eps)
 
    # Run the ECC algorithm. The results are stored in warp_matrix.
    (cc, warp_matrix) = cv.findTransformECC (im1_gray, im2_gray, warp_matrix, cv.MOTION_TRANSLATION, criteria)
 
    # Use warpAffine for Translation, Euclidean and Affine
    im2_aligned = cv.warpAffine(im2, warp_matrix, (sz[1],sz[0]), flags=cv.INTER_LINEAR + cv.WARP_INVERSE_MAP);
    
    return im2_aligned

def image_change(im2, im1):
    "Compute change between frames"
    # size check
    assert im2.shape == im1.shape

    # image registration
    im2 = image_warp(im2, im1)

    im2 = np.array(im2, dtype = 'int32')
    im1 = np.array(im1, dtype = 'int32')

    diff_im = np.abs(im2 - im1)
    diff_im = np.array(diff_im, dtype = 'uint8')  # convert back to 8-bit per channel

    return diff_im

def image_bleed(im, size = 5):
    ##TODO
    pass
