import numpy as np
import cv2 as cv
import data
import image as im
import pickle

x = data.get_dir()
done = 0
for folder in x:
    try:
        f = open('dat/' + folder, 'rb')
    except:
        try:
            print('Folder: ', folder, '\tDone: ', done)
            dat = data.read_dir({folder: x[folder]}, False)[folder]
            changes = []
            for i in range(len(dat) - 1):
                print('\t', i, '/', len(dat) - 1)
                im_change = im.image_change(dat[i + 1][1], dat[i][1])
                changes.append(im.get_cc_im(im_change))
            f = open('dat/' + folder, 'wb')
            pickle.dump(changes, f)
            f.close()
        except:
            print('Folder ', folder, ' FAILED!')
    done += 1
